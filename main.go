package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/http/httptrace"
	"time"

	"golang.org/x/net/http2"
)

func poll() (int, error) {
	req, err := http.NewRequest("GET", "https://gitlab.com", nil)
	if err != nil {
		return 0, err
	}
	var t0, t1, t2, t3, t4 time.Time
	trace := &httptrace.ClientTrace{
		DNSStart: func(_ httptrace.DNSStartInfo) { t0 = time.Now() },
		DNSDone:  func(_ httptrace.DNSDoneInfo) { t1 = time.Now() },
		ConnectStart: func(_, _ string) {
			if t1.IsZero() {
				t1 = time.Now()
			}
		},
		ConnectDone: func(net, addr string, err error) {
			if err != nil {
				log.Fatalf("Unable to connect to host %v: %v", addr, err)
			}
			t2 = time.Now()
			fmt.Printf("Connected to: %v\n", addr)
		},
		GotConn:              func(_ httptrace.GotConnInfo) { t3 = time.Now() },
		GotFirstResponseByte: func() { t4 = time.Now() },
	}

	req = req.WithContext(httptrace.WithClientTrace(context.Background(), trace))

	tr := &http.Transport{
		Proxy:                 http.ProxyFromEnvironment,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}

	host, _, err := net.SplitHostPort(req.Host)
	if err != nil {
		host = req.Host
	}

	tr.TLSClientConfig = &tls.Config{
		ServerName:         host,
		InsecureSkipVerify: false,
	}
	err = http2.ConfigureTransport(tr)
	if err != nil {
		log.Fatalf("failed to prepare transport for HTTP/2: %v", err)
	}

	client := &http.Client{
		Transport: tr,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			// always refuse to follow redirects, visit does that
			// manually if required.
			return http.ErrUseLastResponse
		},
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("failed to read response: %v", err)
	}

	resp.Body.Close()

	t5 := time.Now() // after read body
	if t0.IsZero() {
		// we skipped DNS
		t0 = t1
	}

	fmt.Println(t1.Sub(t0))
	fmt.Println(t2.Sub(t1))
	fmt.Println(t3.Sub(t2))
	fmt.Println(t4.Sub(t3))
	fmt.Println(t5.Sub(t4))
	return 0, err
}

func main() {
	// inputTime := flag.Int("numb", 3000, "Number of seconds to poll for")
	// pollRate := flag.Int("numb", 2, "Seconds between each poll")
	// flag.Parse()

	poll()

}
